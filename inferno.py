import itertools
import string
import threading
from pynput import keyboard
import time
from sys import stderr
import ansi

# Config
wait = 0.01
hotKey = {keyboard.Key.f4, keyboard.KeyCode.from_char('c')}
chars = string.digits
Length = 6
useWordList = False
wordList = "wordlist.txt"

# main
kb = keyboard.Controller()
running = False
thread = None
current = set()


class GuessPassword(threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)
        self.stopped = False

    def run(self):
        global running
        running = True
        attempts = 0
        for _ in range(Length + 1):
            kb.press(keyboard.Key.backspace)
            kb.release(keyboard.Key.backspace)
        if useWordList:
            for line in open(wordList, 'r', encoding='ISO-8859-1').readlines():
                if not self.stopped:
                    attempts += 1
                    kb.type(line)
                    kb.press(keyboard.Key.enter)
                    kb.release(keyboard.Key.enter)
                    time.sleep(wait)
                    for _ in line:
                        kb.press(keyboard.Key.backspace)
                        kb.release(keyboard.Key.backspace)
                    if attempts >= 2:
                        print(ansi.Cursor().Up(), end="")
                        print(ansi.Erase().Line(), end="")
                        print(ansi.Cursor().Up(), end="")
                        print(ansi.Erase().Line(), end="")
                    print(ansi.BrightForeground().GREEN + "attempts: " + str(attempts))
                    print(ansi.BrightForeground().BLUE + "guess: " + str(line))
                    if self.stopped:
                        print(ansi.Cursor().Up(), end="")
                        print(ansi.Erase().Line(), end="")
                        print(ansi.Cursor().Up(), end="")
                        print(ansi.Erase().Line(), end="")
                        running = False
                        break
        else:
            for password_length in range(Length + 1):
                if not self.stopped:
                    for guess in itertools.product(chars, repeat=password_length):
                        attempts += 1
                        guess = ''.join(guess)
                        kb.type(guess)
                        kb.press(keyboard.Key.enter)
                        kb.release(keyboard.Key.enter)
                        time.sleep(wait)
                        for _ in guess:
                            kb.press(keyboard.Key.backspace)
                            kb.release(keyboard.Key.backspace)
                        if attempts >= 2:
                            print(ansi.Cursor().Up(), end="")
                            print(ansi.Erase().Line(), end="")
                            print(ansi.Cursor().Up(), end="")
                            print(ansi.Erase().Line(), end="")
                        print(ansi.BrightForeground().YELLOW + "attempts: " + str(attempts))
                        print(ansi.BrightForeground().RED + "guess: " + str(guess))
                        if self.stopped:
                            print(ansi.Cursor().Up(), end="")
                            print(ansi.Erase().Line(), end="")
                            print(ansi.Cursor().Up(), end="")
                            print(ansi.Erase().Line(), end="")
                            running = False
                            break
        running = False


def on_press(key):
    global thread
    if any([key in hotKey]):
        current.add(key)
        if all(k in current for k in hotKey):
            if not running:
                thread = GuessPassword()
                thread.start()
            else:
                thread.stopped = True


def on_release(key):
    if any([key in hotKey]):
        current.remove(key)


# print(stderr.isatty())


print(
    "\n" +
    ansi.foregroundbit8(214) + ansi.Intensity().DECREASED +
    "██╗███╗   ██╗███████╗███████╗██████╗ ███╗   ██╗ ██████╗ \n"
    "██║████╗  ██║██╔════╝██╔════╝██╔══██╗████╗  ██║██╔═══██╗\n"
    "██║██╔██╗ ██║█████╗  █████╗  ██████╔╝██╔██╗ ██║██║   ██║\n"
    "██║██║╚██╗██║██╔══╝  ██╔══╝  ██╔══██╗██║╚██╗██║██║   ██║\n"
    "██║██║ ╚████║██║     ███████╗██║  ██║██║ ╚████║╚██████╔╝\n"
    "╚═╝╚═╝  ╚═══╝╚═╝     ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝ ╚═════╝ \n"
    + ansi.reset()
)

with keyboard.Listener(on_press=on_press, on_release=on_release) as listener:
    listener.join()
