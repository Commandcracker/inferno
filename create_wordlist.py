import codecs
import re
import os

# Config
input = "input.txt"
final = "final.txt"
lenght = 6
pattern = re.compile("^([0-9]+)+$")

# Main
try:
    os.remove(final)
except OSError:
    pass

with open(input, 'r+', encoding='ISO-8859-1') as f1:
    for line in f1.readlines():
        if len(line) <= lenght+1:
            if pattern.match(line):
                with codecs.open(final, 'a', encoding='ISO-8859-1') as f2:
                    f2.write(line)
